
## A proof-of-concept CLI for creating dark crystal shares of a given secret for sending over [magic wormhole](https://github.com/magic-wormhole/magic-wormhole)

This currently only creates a set of share files, or recovers a secret from a set of share files. You currently would have to manually send them to your chosen custodians over magic wormhole. But it gives an idea of how a possible integration between Dark Crystal and Magic Wormhole could be built.

See https://github.com/LeastAuthority/magic-crystal/blob/main/protocol.md

### To create share files in the current directory

`cargo run -- share --secret "beep boop" --n 5 --threshold 3`

### To recover from a set of share files in the current directory

`cargo run -- combine`
