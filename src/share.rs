use dark_crystal_key_backup_rust::{
    combine_authenticated, default_threshold, share_and_encrypt_detached_nonce,
    share_authenticated, EncryptedShareSet, Error, RecoveryError,
};
use std::collections::HashMap;
use std::collections::HashSet;
use std::fmt;
use std::fs::{self, File};
use std::io::prelude::*;

const SHARE_LENGTH: usize = 33;

/// Generate shares and write to files
/// Currently this will clobber existing files with the same name without warning
pub fn write_share_files(secret: Vec<u8>, n: u8, threshold: u8) -> std::io::Result<()> {
    let (shares, ciphertext) = share_authenticated(&secret[..], n, threshold).unwrap();

    for share in shares.iter() {
        let index = &share[0].clone();
        let mut file = File::create(format!("{}.share", index))?;
        file.write_all(&share)?;
        file.write_all(&ciphertext)?;
    }
    Ok(())
}

/// Read a set of shares from the current directory
pub fn read_share_files() -> std::io::Result<Vec<RecoveredShare>> {
    let mut recovered_shares: Vec<RecoveredShare> = Vec::new();
    for entry in fs::read_dir(".")? {
        if let Ok(entry) = entry {
            if let Ok(file_type) = entry.file_type() {
                if file_type.is_file() {
                    if entry
                        .file_name()
                        .to_str()
                        .expect("Cannot parse filename")
                        .ends_with(".share")
                    {
                        println!("Reading {:?}", entry.file_name());
                        let mut file = File::open(entry.path())?;
                        let mut share = Vec::<u8>::new();
                        file.read_to_end(&mut share)?;
                        recovered_shares.push(RecoveredShare::from_concat(&share));
                    }
                }
            }
        }
    }
    Ok(recovered_shares)
}

/// A plaintext share together with associated ciphertext
#[derive(Debug)]
pub struct RecoveredShare {
    share: Vec<u8>,
    ciphertext: Vec<u8>,
}

impl RecoveredShare {
    pub fn from_concat(share_and_ciphertext: &Vec<u8>) -> Self {
        // TODO handle the case where share_and_ciphertext.len() < SHARE_LENGTH
        RecoveredShare {
            share: (&share_and_ciphertext[..SHARE_LENGTH]).to_vec(),
            ciphertext: (&share_and_ciphertext[SHARE_LENGTH..]).to_vec(),
        }
    }
}

impl fmt::Display for RecoveredShare {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}{}",
            hex::encode(&self.share),
            hex::encode(&self.ciphertext)
        )
    }
}

/// Attempt recovery for a set of shares, handling the case that
/// shares from different share sets are mixed together,
/// and also handling duplicate shares.
/// This will only return the first secret found. If there is more
/// than one complete set, multiple secrets will not be recovered
pub fn recover(recovered_shares: Vec<RecoveredShare>) -> Result<Vec<u8>, RecoveryError> {
    let mut share_sets: HashMap<Vec<u8>, HashSet<Vec<u8>>> = HashMap::new();
    for recovered_share in recovered_shares.iter() {
        let share_set = share_sets
            .entry(recovered_share.ciphertext.clone())
            .or_insert(HashSet::new());
        share_set.insert(recovered_share.share.clone());
    }
    // For each shareset with > 1 share, attempt recovery
    for (ciphertext, share_set) in share_sets.iter() {
        if share_set.len() > 1 {
            // TODO this is probably not the best way to convert HashSet to vector
            let share_set_vec: Vec<Vec<u8>> = share_set.into_iter().map(|s| s.clone()).collect();
            if let Ok(secret) = combine_authenticated(share_set_vec, ciphertext.to_vec()) {
                return Ok(secret);
            }
        }
    }
    Err(RecoveryError {
        message: "Cannot recover secret".to_string(),
    })
}
