use clap::{Parser, Subcommand};
use colored::*;
mod share;

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Cli {
    #[clap(subcommand)]
    command: Commands,
}

#[derive(Subcommand, Debug)]
enum Commands {
    /// Create a share set
    #[clap(arg_required_else_help = true)]
    Share {
        /// The secret
        #[clap(short, long)]
        secret: String,

        /// Number of shares
        #[clap(short, long)]
        n: u8,

        /// Threshold
        #[clap(short, long)]
        threshold: u8,
    },

    /// Recover a secret
    Combine {},
}

fn main() -> std::io::Result<()> {
    let args = Cli::parse();

    match &args.command {
        Commands::Share {
            secret,
            n,
            threshold,
        } => {
            share::write_share_files(secret.as_bytes().to_vec(), n.clone(), threshold.clone())?;
            println!("Share files created");
        }

        Commands::Combine {} => {
            let recovered_shares = share::read_share_files()?;
            println!("Read {} share files", recovered_shares.len());
            match share::recover(recovered_shares) {
                Ok(secret) => println!(
                    "Recovered secret {}",
                    std::str::from_utf8(&secret).unwrap().green()
                ),
                Err(err) => println!("{}", format!("{}", err).red()),
            }
        }
    }
    Ok(())
}
